from django.contrib import admin

from django.contrib import admin
from .models import Book

class BookAdmin(admin.ModelAdmin):
	list_display = ['Slackname','Book_Title','Genre','Author','Source_Link','created_at']

admin.site.register(Book, BookAdmin)

# Register your models here.
