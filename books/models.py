from django.db import models
class Book(models.Model):
    Slackname = models.CharField(max_length=100)
    Book_Title = models.CharField(max_length=100)
    Genre = models.CharField(max_length=100)
    Author = models.CharField(max_length=100)
    Source_Link = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)


# Create your models here.
