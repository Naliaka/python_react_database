import React, { Component } from "react";
import PropTypes from "prop-types";

class Form extends Component {
  static propTypes = {
    endpoint: PropTypes.string.isRequired
  };

  state = {
    Slackname: "",
    Book_Title: "",
    Genre: "",
    Author: "",
    Source_Link : "",
  };

  handleChange = e => {
    this.setState({ [e.target.Slackname]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { Slackname,Book_Title,Genre,Author,Source_Link } = this.state;
    const book = { Slackname,Book_Title,Genre,Author,Source_Link };
    const conf = {
      method: "post",
      body: JSON.stringify(book),
      headers: new Headers({ "Content-Type": "application/json" })
    };
    fetch(this.props.endpoint, conf).then(response => console.log(response));
  };

  render() {
    const { Slackname, Book_Title,Genre,Author,Source_Link} = this.state;
    return (
      <div className="column">
        <form onSubmit={this.handleSubmit}>
          <div className="field">
            <label className="label">Slackname</label>
            <div className="control">
              <input
                className="input"
                type="text"
                name="Slackname"
                onChange={this.handleChange}
                value={Slackname}
                required
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Book_Title</label>
            <div className="control">
              <input
                className="input"
                type="Book_Title"
                name="Book_Title"
                onChange={this.handleChange}
                value={Book_Title}
                required
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Genre</label>
            <div className="control">
              <textarea
                className="textarea"
                type="text"
                name="Genre"
                onChange={this.handleChange}
                value={Genre}
                required
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Author</label>
            <div className="control">
              <textarea
                className="textarea"
                type="text"
                name="Author"
                onChange={this.handleChange}
                value={Author}
                required
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Source_Link</label>
            <div className="control">
              <textarea
                className="textarea"
                type="text"
                name="Source_Link"
                onChange={this.handleChange}
                value={Source_Link}
                required
              />
            </div>
          </div>
          <div className="control">
            <button type="submit" className="button is-info">
              Add Book
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Form;